<?php

/**
 * Custom amendments for the theme.
 *
 * @category   Genesis_Sandbox
 * @package    Functions
 * @subpackage Functions
 * @author     Travis Smith and Jonathan Perez
 * @license    http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link       http://surefirewebservices.com/
 * @since      1.1.0
 */

// Initialize Sandbox ** DON'T REMOVE **
require_once( get_stylesheet_directory() . '/lib/init.php');

add_action( 'genesis_setup', 'gs_theme_setup', 15 );

//Theme Set Up Function
function gs_theme_setup() {

	// Remove the entry meta in the entry footer (requires HTML5 theme support)
	remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

	// Remove the entry meta in the entry header (requires HTML5 theme support)
	remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );

	// Unregister secondary sidebar
	unregister_sidebar( 'sidebar-alt' );

	// Remove the entire footer of Genesis
	remove_action( 'genesis_footer', 'genesis_do_footer' );
	
	//Enable HTML5 Support
	add_theme_support( 'html5' );


	/** 
	 * 01 Set width of oEmbed
	 * genesis_content_width() will be applied; Filters the content width based on the user selected layout.
	 *
	 * @see genesis_content_width()
	 * @param integer $default Default width
	 * @param integer $small Small width
	 * @param integer $large Large width
	 */
	$content_width = apply_filters( 'content_width', 600, 430, 920 );
	
	//Custom Image Sizes
	add_image_size( 'featured-image', 225, 160, TRUE );
	
	// Enable Custom Background
	//add_theme_support( 'custom-background' );

	// Enable Custom Header
	//add_theme_support('genesis-custom-header');

	// Remove the site description
	remove_action( 'genesis_site_description', 'genesis_seo_site_description' );



	// Add support for structural wraps
	add_theme_support( 'genesis-structural-wraps', array(
		'header',
		'nav',
		'subnav',
		'inner',
		// 'footer-widgets',
		'footer'
	) );

	/**
	 * 07 Footer Widgets
	 * Add support for 3-column footer widgets
	 * Change 3 for support of up to 6 footer widgets (automatically styled for layout)
	 */
	// add_theme_support( 'genesis-footer-widgets', 3 );

	/**
	 * 08 Genesis Menus
	 * Genesis Sandbox comes with 4 navigation systems built-in ready.
	 * Delete any menu systems that you do not wish to use.
	 */
	add_theme_support(
		'genesis-menus', 
		array(
			'primary'   => __( 'Primary Navigation Menu', CHILD_DOMAIN ), 
			// 'secondary' => __( 'Secondary Navigation Menu', CHILD_DOMAIN ),
			'footer'    => __( 'Footer Navigation Menu', CHILD_DOMAIN ),
			'mobile'    => __( 'Mobile Navigation Menu', CHILD_DOMAIN ),
		)
	);
	
	// Add Mobile Navigation
	add_action( 'genesis_before', 'gs_mobile_navigation', 5 );
	
	//Enqueue Sandbox Scripts
	add_action( 'wp_enqueue_scripts', 'gs_enqueue_scripts' );
	
	/**
	 * 13 Editor Styles
	 * Takes a stylesheet string or an array of stylesheets.
	 * Default: editor-style.css 
	 */
	//add_editor_style();
	
	
	// Register Sidebars
	gs_register_sidebars();
	
} // End of Set Up Function

// Register Sidebars
function gs_register_sidebars() {
	$sidebars = array(
		array(
			'id'			=> 'home-content',
			'name'			=> __( 'Home Content', CHILD_DOMAIN ),
			'description'	=> __( 'This is the homepage content section.', CHILD_DOMAIN ),
		),
		array(
			'id'			=> 'footer-widget',
			'name'			=> __( 'Footer', CHILD_DOMAIN ),
			'description'	=> __( 'Widget area on the footer.', CHILD_DOMAIN ),
		)
	);
	
	foreach ( $sidebars as $sidebar )
		genesis_register_sidebar( $sidebar );
}

/**
 * Enqueue and Register Scripts - Twitter Bootstrap, Font-Awesome, and Common.
 */
require_once('lib/scripts.php');

/**
 * Add navigation menu 
 * Required for each registered menu.
 * 
 * @uses gs_navigation() Sandbox Navigation Helper Function in gs-functions.php.
 */

//Add Mobile Menu
function gs_mobile_navigation() {
	
	$mobile_menu_args = array(
		'echo' => true,
	);
	
	gs_navigation( 'mobile', $mobile_menu_args );
}

// Add Widget Area After Post
add_action('genesis_after_entry', 'gs_do_after_entry');
function gs_do_after_entry() {
 	if ( is_single() ) {
 	genesis_widget_area( 
                'after-post', 
                array(
                        'before' => '<aside id="after-post" class="after-post"><div class="home-widget widget-area">', 
                        'after' => '</div></aside><!-- end #home-left -->',
                ) 
        );
 	}
}

// Remove frontpage title
add_action('get_header', 'remove_page_title');
function remove_page_title() {
  if ( is_front_page() ) {
    remove_action('genesis_entry_header', 'genesis_do_post_title');
  }
}

//* Custom footer
add_action( 'genesis_footer', 'clm_custom_footer' );
function clm_custom_footer() {
	?>
	<!-- <div class="container"> -->
		<div class="row">
			<div class="col-md-8">
				<?php
					$args = array(
							'theme_location'  => 'footer',
							'container'       => 'nav',
							'container_class' => 'footer-menu',
							'menu_class'      => 'menu genesis-nav-menu menu-footer',
							'depth'           => 1,
						);
					wp_nav_menu( $args );
				?>
				<div class="social-buttons">		
					<span class="gplus-button"><g:plusone size="medium"></g:plusone></span>
					<span class="fb-like" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false"></span>
					<span class="twitter-share"><a href="https://twitter.com/share" class="twitter-share-button">Tweet</a></span>
				</div>
				<div class="copyright">
					<p>Copyright 2015, Car Loan Market, LLC.</p>
					<p>All rights reserved.</p>
				</div>		
			</div>
			<div class="col-md-4">
				<div class="geotrust-bbb-buttons">
					<span height="60" width="115">
						<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript" SRC="//smarticon.geotrust.com/si.js"></SCRIPT>
					</span>
					<span>
						<a target="_blank" href="http://carloanmarket.shopmybbb.com">
							<img alt="Better Business Bureau" src="<?php echo get_stylesheet_directory_uri(); ?>/images/bbb.jpg" />
						</a>
					</span>
				</div>
			</div>
		</div>
	<!-- </div> -->
	<?php
}

// Add Google+
add_action('wp_head', 'clm_googleplusone');
function clm_googleplusone() {
	echo '<script src="https://apis.google.com/js/platform.js" async defer></script>';
}

// Add Facebook Like
add_action( 'wp_head', 'clm_facebook' );
function clm_facebook() {
	?>
	<script>
		(function(d, s, id) {
	        var js, fjs = d.getElementsByTagName(s)[0];
	        if (d.getElementById(id)) return;
	        js = d.createElement(s); js.id = id;
	        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
	        fjs.parentNode.insertBefore(js, fjs);
	    } (document, 'script', 'facebook-jssdk'));
    </script>
	<?php
}

// Twitter Share
add_action( 'wp_footer', 'clm_twitter');
function clm_twitter() {
	?>
	<script>
		!function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = "//platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } } (document, "script", "twitter-wjs");
	</script>
	<?php
}

// Add custom post info
add_action( 'genesis_entry_header', 'clm_post_info', 12 );
function clm_post_info() {
	if ( is_single() ) {
		?>
		<div class="social-buttons">		
			<span class="gplus-button"><g:plusone size="medium"></g:plusone></span>
			<span class="fb-like" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false"></span>
			<span class="twitter-share"><a href="https://twitter.com/share" class="twitter-share-button">Tweet</a></span>
		</div>
		<div class="back-to-articles"><a href="/articles/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/BacktoArticles.jpg" alt="Back to Articles"></a></div>
		<?php
	}
}

// Add custom post meta
add_action( 'genesis_entry_footer', 'clm_post_meta', 12 );
function clm_post_meta() {
	if ( is_single() ) {
		?>
			<div class="back-to-articles">
				<a href="/articles/">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/BacktoArticles.jpg" alt="Back to Articles">
				</a>
			</div>
			<div class="entry-date"><?php the_date(); ?></div>
		<?php
	}
}

// Sub-footer widget area
add_action( 'genesis_after_footer', 'clm_footer_widget_area' );
function clm_footer_widget_area() {
	if( !is_front_page() ) {
		if ( is_active_sidebar( 'footer-widget' ) ) {
			echo "<aside class=\"footer-widget widget-area\"><div class=\"wrap\">";
	        dynamic_sidebar('footer-widget');
	        echo "</div></aside>";
	    }
	}
}
